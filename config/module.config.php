<?php

use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use UnicaenAuth\Guard\PrivilegeController;
use UnicaenFaq\Controller\Factory\FaqControllerFactory;
use UnicaenFaq\Form\Factory\FaqFormFactory;
use UnicaenFaq\Provider\Privilege\FaqPrivileges;
use UnicaenFaq\Service\FaqServiceFactory;

return array(
    'unicaen-faq' => [
        /*
         * Masque des chemins vers les scripts SQL de création du schéma de base de données sur lequel repose ce module.
         */
        'db_schema_glob_paths' => __DIR__ . '/../data/*.sql',
    ],
    'doctrine'     => [
        'driver'     => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenFaq\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/UnicaenFaq/Entity/Db/Mapping',
                ],
            ],
        ],
    ],
    'bjyauthorize'    => [
        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
                'Faq' => [],
            ],
        ],
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => 'UnicaenFaq\Controller\Index',
                    'action'     => [
                        'index',
                    ],
                    'roles' => ['guest'], // pas d'authentification requise
                ],
                [
                    'controller' => 'UnicaenFaq\Controller\Index',
                    'action'     => [
                        'ajouter',
                        'modifier',
                        'supprimer',
                    ],
                    'privileges' => [FaqPrivileges::FAQ_MODIFICATION]
                ],
            ],
        ],
    ],
    'router' => [
        'routes' => [
            'faq' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/faq',
                    'defaults' => [
                        'controller' => 'UnicaenFaq\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'ajouter' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/ajouter',
                            'defaults' => [
                                'action' => 'ajouter',
                            ],
                        ],
                    ],
                    'modifier' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/:faq/modifier',
                            'constraints' => [
                                'faq' => '\d+',
                            ],
                            'defaults' => [
                                'action' => 'modifier',
                            ],
                        ],
                    ],
                    'supprimer' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/:faq/supprimer',
                            'constraints' => [
                                'faq' => '\d+',
                            ],
                            'defaults' => [
                                'action' => 'supprimer',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'faq' => [
                        'label' => 'FAQ',
                        'route' => 'faq',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            'UnicaenFaq\Service\FaqService' => FaqServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            'UnicaenFaq\Controller\Index' => FaqControllerFactory::class,
        ],
    ],
    'form_elements'   => [
        'factories' => [
            'UnicaenFaq\Form\FaqForm' => FaqFormFactory::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'unicaen-faq/index'    => __DIR__ . '/../view/unicaen-faq/index.phtml',
            'unicaen-faq/modifier' => __DIR__ . '/../view/unicaen-faq/modifier.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'public_files' => [
        'inline_scripts' => [
            '902_' => '//gest.unicaen.fr/public/tinymce-4.4.1/js/tinymce/tinymce.min.js',
        ],
        'stylesheets'    => [
//            '112_' => 'https://gest.unicaen.fr/public/font-awesome-4.5.0/css/font-awesome.min.css',
//            '113_' => 'https://gest.unicaen.fr/public/open-sans-gh-pages/open-sans.css',
//            '120_' => 'css/callout.css',
//            '121_' => 'https://gest.unicaen.fr/public/DataTables-1.10.12/media/css/dataTables.bootstrap.min.css',
        ],
    ],
);