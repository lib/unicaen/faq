<?php

namespace UnicaenFaq\Service;

interface FaqServiceAwareInterface
{
    public function setFaqService(FaqService $service);
}