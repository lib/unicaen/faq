<?php

namespace UnicaenFaq\Service;

trait FaqServiceAwareTrait
{
    /**
     * @var FaqService
     */
    protected $faqService;

    /**
     * @param FaqService $service
     */
    public function setFaqService(FaqService $service)
    {
        $this->faqService = $service;
    }
}