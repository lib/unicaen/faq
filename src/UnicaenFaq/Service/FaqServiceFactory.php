<?php

namespace UnicaenFaq\Service;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Exception\InvalidArgumentException;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

final class FaqServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    private function getEntityManager(ContainerInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');

        if (!isset($config['unicaen-faq']['entity_manager_name'])) {
            throw new InvalidArgumentException("Option de config 'unicaen-faq.entity_manager_name' introuvable");
        }

        $name = $config['unicaen-faq']['entity_manager_name'];

        /** @var EntityManager $em */
        $em = $serviceLocator->get("doctrine.entitymanager.$name");

        return $em;
    }

    private function getFaqEntityClass(ContainerInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');

        if (!isset($config['unicaen-faq']['faq_entity_class'])) {
            throw new InvalidArgumentException("Option de config 'unicaen-faq.faq_entity_class' introuvable");
        }

        return $config['unicaen-faq']['faq_entity_class'];
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $faqEntityClass = $this->getFaqEntityClass($container);

        if (!class_exists($faqEntityClass)) {
            throw new InvalidArgumentException("La classe spécifiée dans l'option de 'unicaen-faq.faq_entity_class' n'existe pas");
        }

        $em = $this->getEntityManager($container);

        $service = new FaqService($em, $faqEntityClass);

        return $service;
    }
}