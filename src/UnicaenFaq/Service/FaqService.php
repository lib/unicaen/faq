<?php

namespace UnicaenFaq\Service;

use Doctrine\DBAL\Exception\DriverException;
use Doctrine\ORM\EntityManager;
use UnicaenApp\Exception\LogicException;
use UnicaenFaq\Entity\Db\Faq;
use Doctrine\ORM\EntityRepository;
use UnicaenApp\Service\EntityManagerAwareInterface;
use UnicaenApp\Service\EntityManagerAwareTrait;

class FaqService implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    protected $faqEntityClass;

    /**
     * FaqService constructor.
     *
     * @param EntityManager $entityManager
     * @param string        $faqEntityClass
     */
    public function __construct(EntityManager $entityManager, $faqEntityClass)
    {
        $this->setEntityManager($entityManager);

        $this->faqEntityClass = $faqEntityClass;
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->entityManager->getRepository($this->faqEntityClass);
    }

    /**
     * Vérifie le schéma de bdd requis par ce service.
     */
    public function checkSchema()
    {
//        $tableName = $this->entityManager->getClassMetadata($this->faqEntityClass)->getTableName();
//        $q = "select count(*) from ALL_TABLES where UPPER(TABLE_NAME) = UPPER(:tablename)";
//        $s = $this->entityManager->getConnection()->executeQuery($q, ['tablename' => $tableName]);
//        $count = (int) $s->fetchColumn(0);
//        if ($count === 0) {
//            throw new LogicException("La table $tableName est introuvable");
//        }
    }

    /**
     * Supprime définitivement une question-réponse.
     *
     * @param Faq $faq
     */
    public function delete(Faq $faq)
    {
        $this->entityManager->remove($faq);
        $this->entityManager->flush($faq);
    }

    /**
     * Persiste la nouvelle question-réponse spécifiée.
     *
     * @param Faq $faq
     */
    public function create(Faq $faq)
    {
        $this->entityManager->persist($faq);
        $this->entityManager->flush($faq);
    }

    /**
     * Persiste les modifications apportées à la question-réponse spécifiée.
     *
     * @param Faq $faq
     */
    public function update(Faq $faq)
    {
        $this->entityManager->flush($faq);
    }
}