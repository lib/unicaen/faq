<?php

namespace UnicaenFaq\Provider\Privilege;

/**
 * Liste des privilèges.
 */
class FaqPrivileges
{
    const FAQ_MODIFICATION = 'faq-modification';
}