<?php

namespace UnicaenFaq\Form;

use UnicaenFaq\Entity\Db\Faq;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;
use Zend\InputFilter\Factory;

class FaqForm extends Form
{
    /**
     * NB: hydrateur injecté par la factory
     */
    public function init()
    {
        $this->setObject(new Faq());

        $this->add(new Text('id'));

        $this->add(
            (new Textarea('question'))
                ->setLabel("Question :")
        );

        $this->add(
            (new Textarea('reponse'))
                ->setLabel("Réponse :")
        );

        $this->add(
            (new Text('ordre'))
                ->setLabel("Ordre :")
        );

        $this->add((
            new Submit('submit'))
                ->setValue("Enregistrer")
                ->setAttribute('class', 'btn btn-primary')
        );

        $this->setInputFilter((new Factory())->createInputFilter([
            'question' => [
                'name' => 'question',
                'required' => true,
            ],
            'reponse' => [
                'name' => 'reponse',
                'required' => true,
            ],
            'ordre' => [
                'name' => 'ordre',
                'required' => false,
            ],
        ]));
    }
}