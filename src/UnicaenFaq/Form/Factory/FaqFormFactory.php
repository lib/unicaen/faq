<?php

namespace UnicaenFaq\Form\Factory;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Interop\Container\ContainerInterface;
use UnicaenFaq\Form\FaqForm;
use Zend\Hydrator\HydratorPluginManager;

class FaqFormFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var DoctrineObject $hydrator */
        $hydrator = $container->get(HydratorPluginManager::class)->get(DoctrineObject::class);

        $form = new FaqForm();
        $form->setHydrator($hydrator);
        
        return $form;
    }
}