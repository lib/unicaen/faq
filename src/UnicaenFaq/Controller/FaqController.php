<?php

namespace UnicaenFaq\Controller;

use Doctrine\ORM\Tools\Pagination\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator;
use UnicaenFaq\Entity\Db\Faq;
use UnicaenFaq\Form\FaqForm;
use UnicaenFaq\Service\FaqServiceAwareInterface;
use UnicaenFaq\Service\FaqServiceAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class FaqController extends AbstractActionController implements FaqServiceAwareInterface
{
    use FaqServiceAwareTrait;

    /**
     * @var FaqForm
     */
    protected $faqForm;

    /**
     * FaqController constructor.
     *
     * @param FaqForm $faqForm
     */
    public function __construct(FaqForm $faqForm)
    {
        $this->faqForm = $faqForm;
    }

    public function indexAction()
    {
        $qb = $this->faqService->getRepository()->createQueryBuilder('f');
        $qb->addOrderBy('f.ordre');

        $paginator = new \Zend\Paginator\Paginator(new DoctrinePaginator(new Paginator($qb, true)));
        $paginator->setItemCountPerPage(-1);

        $viewModel = new ViewModel([
            'faqs' => $paginator,
        ]);
        $viewModel->setTemplate('unicaen-faq/index');

        return $viewModel;
    }

    public function modifierAction()
    {
        $faq = $this->requestedFaq();
        $this->faqForm->bind($faq);

        if ($data = $this->params()->fromPost()) {
            $this->faqForm->setData($data);
            if ($this->faqForm->isValid()) {
                /** @var Faq $faq */
                $faq = $this->faqForm->getData();
                $this->faqService->update($faq);

                $this->flashMessenger()->addSuccessMessage("Question-réponse modifiée avec succès");

                return $this->redirect()->toRoute('faq', [], ['query' => ['selected' => $faq->getId()]], true);
            }
        }

        $this->faqForm->setAttribute('action', $this->url()->fromRoute('faq/modifier', [], [], true));

        $viewModel = new ViewModel([
            'form' => $this->faqForm,
        ]);
        $viewModel->setTemplate('unicaen-faq/modifier');

        return $viewModel;
    }

    public function ajouterAction()
    {
        if ($data = $this->params()->fromPost()) {
            $this->faqForm->setData($data);
            if ($this->faqForm->isValid()) {
                /** @var Faq $faq */
                $faq = $this->faqForm->getData();
                $this->faqService->create($faq);

                $this->flashMessenger()->addSuccessMessage("Question-réponse ajoutée avec succès");

                return $this->redirect()->toRoute('faq', [], ['query' => ['selected' => $faq->getId()]], true);
            }
        }

        $this->faqForm->setAttribute('action', $this->url()->fromRoute('faq/ajouter'));

        $viewModel = new ViewModel([
            'form' => $this->faqForm,
        ]);
        $viewModel->setTemplate('unicaen-faq/modifier');

        return $viewModel;
    }

    public function supprimerAction()
    {
        $faq = $this->requestedFaq();
        $this->faqService->delete($faq);

        $this->flashMessenger()->addSuccessMessage("Question-réponse supprimée avec succès");

        return $this->redirect()->toRoute('faq', [], [], true);
    }

    /**
     * @return Faq
     */
    protected function requestedFaq()
    {
        $id = $this->params()->fromRoute('faq');
        if (!$id) {
            return null;
        }

        /** @var Faq $faq */
        $faq = $this->faqService->getRepository()->find($id);

        return $faq;
    }
}