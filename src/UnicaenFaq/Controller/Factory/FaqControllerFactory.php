<?php

namespace UnicaenFaq\Controller\Factory;

use Interop\Container\ContainerInterface;
use UnicaenFaq\Controller\FaqController;
use UnicaenFaq\Form\FaqForm;
use UnicaenFaq\Service\FaqService;
use Zend\ServiceManager\ServiceLocatorInterface;

class FaqControllerFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @return FaqController
     */
    public function __invoke(ContainerInterface $container)
    {
        $faqService = $this->getFaqService($container);

        /** @var FaqForm $form */
        $form = $container->get('FormElementManager')->get('UnicaenFaq\Form\FaqForm');

        $controller = new FaqController($form);
        $controller->setFaqService($faqService);

        return $controller;
    }

    private function getFaqService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var FaqService $service */
        $service = $serviceLocator->get('UnicaenFaq\Service\FaqService');

        return $service;
    }
}