<?php

namespace UnicaenFaq\Entity\Db;

use Zend\Permissions\Acl\Resource\ResourceInterface;

/**
 * Entité représentant un couple question-réponse.
 */
class Faq implements ResourceInterface
{
    const RESOURCE_ID = 'Faq';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $question;

    /**
     * @var string
     */
    private $reponse;

    /**
     * @var integer
     */
    private $ordre;

    /**
     * Set contenu
     *
     * @param string $question
     * @return self
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set nom
     *
     * @param string $reponse
     * @return self
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * @param int $ordre
     * @return Faq
     */
    public function setOrdre($ordre)
    {
        $this->ordre = (int) $ordre;

        return $this;
    }


    /**
     * Returns the string identifier of the Resource
     *
     * @return string
     */
    public function getResourceId()
    {
        return self::RESOURCE_ID;
    }
}