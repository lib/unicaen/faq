--
-- Table FAQ.
--
CREATE TABLE FAQ (
  ID       NUMBER(38, 0),
  QUESTION VARCHAR2(2000 CHAR) NOT NULL,
  REPONSE  VARCHAR2(2000 CHAR) NOT NULL,
  ORDRE    NUMBER(38, 0),
  CONSTRAINT FAQ_PK PRIMARY KEY (ID)
);

CREATE SEQUENCE FAQ_ID_SEQ;




--
-- Jeu d'essai.
--
INSERT INTO FAQ(ID, QUESTION, REPONSE, ORDRE) VALUES
  (FAQ_ID_SEQ.nextval, 'Qu''est-ce qu''une question ?', 'C''est une phrase appelant une réponse.', 10);





--
-- Création si besoin de la catégorie de privilège 'faq'.
--
insert into CATEGORIE_PRIVILEGE(ID, CODE, LIBELLE, ORDRE) values
  (CATEGORIE_PRIVILEGE_ID_SEQ.nextval, 'faq', 'FAQ', 10);
--
-- Privilège 'modification'.
--
insert into PRIVILEGE(ID, CATEGORIE_ID, CODE, LIBELLE, ORDRE)
  select privilege_id_seq.nextval, cp.id, 'modification', 'Modification de la FAQ', 10
  from CATEGORIE_PRIVILEGE cp where cp.CODE = 'faq';
