--
-- Table FAQ.
--
create table faq (
  id       serial not null constraint faq_pk primary key,
  question text not null,
  reponse  text not null,
  ordre    smallint
);

create unique index faq_id_uindex on faq (id);


--
-- Jeu d'essai.
--
insert into faq(question, reponse, ordre) values
  ('qu''est-ce qu''une question ?'::text, 'c''est une phrase appelant une réponse.'::text, 10);

/*
--
-- création si besoin de la catégorie de privilège 'faq'.
--
insert into categorie_privilege(code, libelle, ordre) values
  ('faq', 'faq', 10);
--
-- privilège 'modification'.
--
insert into privilege(categorie_id, code, libelle, ordre)
  select cp.id, 'modification', 'modification de la faq', 10
  from categorie_privilege cp where cp.code = 'faq';
*/