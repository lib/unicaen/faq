<?php

namespace UnicaenFaq;

use UnicaenApp\Exception\RuntimeException;
use UnicaenFaq\Service\FaqService;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $application = $e->getApplication();
        $application->getServiceManager()->get('translator');
        $eventManager = $application->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $this->checkModuleRequirements($e);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function checkModuleRequirements(MvcEvent $event)
    {
        $sl = $event->getApplication()->getServiceManager();

        /** @var FaqService $service */
        $service = $sl->get('UnicaenFaq\Service\FaqService');

        try {
            $service->checkSchema();
        }
        catch (\Exception $e) {
            $config = $sl->get('config');
            $pathsArray = glob($config['unicaen-faq']['db_schema_glob_paths']);
            throw new RuntimeException(sprintf(
                "Vous avez activé le module UnicaenFaq mais le schéma de base de données est incorrect: %s. " .
                "Reportez-vous aux scripts suivants : %s.",
                $e->getMessage(),
                implode(', ', array_map('realpath', $pathsArray))
            ));
        }
    }
}
